import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_monitoring_services_running_and_enabled(Service):
    for item in ('delete-non-jenkins-epics-release.service', 'epics-gid-write.service'):
        service = Service(item)
        assert service.is_running
        assert service.is_enabled
